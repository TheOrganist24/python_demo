import math

def greet(name):
    """Prints a greeting
    """
    print("Hello", name)


def hypo(x, y):
    """Calculates the hypotenuse
    """
    z = math.sqrt(x**2 + y**2)
    print(z)


def generate_four_by_three():
    """Generates a four by three array
    
    Array is populated with the numbers 1, 2, and 3
    """
    master = []
    for n in range(1,5):
        sublist = []
        for m in range(1,4):
            sublist.append(m)
        master.append(sublist)
    return master
    
    
def daddy_func(array):
    """Processes 2-dimensional arrays
    
    This function takes a 2-dimensional array and for every element adapts it.
    If the element is even the element is halved, if it is odd, the element
    gets muliplied by 3 and has 1 added to it.
    """
    for i, item in enumerate(array):
        for j, jtem in enumerate(item):
            if jtem % 2 == 0:
                array[i][j] = jtem / 2
            else:
                array[i][j] = 2 * jtem + 1
    return array

