import demomodule as demo

# generate 4x3 array using function from demomodule
demo_list = demo.generate_four_by_three()
print("Generated list is:", demo_list)

# process array according to function from demomodule
result = demo.daddy_func(demo_list)
print("Resulting list is:", result)
